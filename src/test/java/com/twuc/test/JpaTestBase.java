package com.twuc.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.function.Consumer;

@DataJpaTest(showSql = false)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public abstract class JpaTestBase {
    @Autowired
    private EntityManager entityManager;

    private EntityManager getEntityManager() {
        return entityManager;
    }

    protected void flush(Consumer<EntityManager> consumer) {
        final EntityManager em = getEntityManager();
        consumer.accept(em);
        em.flush();
    }

    protected void flushAndClear(Consumer<EntityManager> consumer) {
        final EntityManager em = getEntityManager();
        consumer.accept(em);
        em.flush();
        em.clear();
    }

    protected void run(Consumer<EntityManager> consumer) {
        final EntityManager em = getEntityManager();
        consumer.accept(em);
    }
}
