package com.twuc.test;

import com.twuc.test.domain.Product;
import com.twuc.test.domain.ProductLine;
import com.twuc.test.repository.ProductLineRepository;
import com.twuc.test.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;


public class ProductControllerTest extends JpaTestBase{
    @Autowired
    private EntityManager entityManager;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductLineRepository productLineRepository;

    @Test
    void test_1() {
        List<Product> products = productRepository.findAll(Sort.by(Sort.Direction.ASC,"productCode"));
    }

    @Test
    void test_2() {
        Product product = new Product("S18_13","duzeyan", new ProductLine("ddd","zzz"),"111","sss","sss",(short)123, new BigDecimal(123),new BigDecimal(123));

        productRepository.save(product);
        entityManager.flush();
        entityManager.clear();

    }

    @Test
    void test_3() {
        //ProductLine productLine = new ProductLine("dada","dsdsd");
        List<ProductLine> productLines = productLineRepository.findAll();
        entityManager.flush();
        entityManager.clear();

    }

    @Test
    void test_4() {

    }
}
