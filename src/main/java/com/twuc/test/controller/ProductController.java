package com.twuc.test.controller;


import com.twuc.test.contract.ProductLineRequest;
import com.twuc.test.contract.ProductRequest;
import com.twuc.test.domain.Product;
import com.twuc.test.domain.ProductLine;
import com.twuc.test.repository.ProductLineRepository;
import com.twuc.test.repository.ProductRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;

@RestController
@RequestMapping("api/")
public class ProductController {

    private final ProductRepository productRepository;

    private final ProductLineRepository productLineRepository;

    Map<String,String> nameMap = new HashMap<>();

    public ProductController(ProductLineRepository productLineRepository, ProductRepository productRepository) {
        this.productLineRepository = productLineRepository;
        this.productRepository = productRepository;

        nameMap.put("description","textDescription");
    }

    @PostMapping(("/products"))
    public ResponseEntity createProduct(@RequestBody ProductRequest productRequest) {
        Product product = new Product();
        BeanUtils.copyProperties(productRequest,product);
        productRepository.save(product);
//        productRepository.save(new Product(productRequest.getCode(),
//                productRequest.getName(),
//                new ProductLine(productRequest.getProductLine(),productRequest.getDescription()),
//                productRequest.getScale(),
//                productRequest.getVendor(),
//                productRequest.getDescription(),
//                productRequest.getQuantityInStock(),
//                productRequest.getBuyPrice(),
//                productRequest.getMSRP()));

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PostMapping("/product-lines")
    public ResponseEntity createProductLine(@RequestBody ProductLineRequest productLineRequest) {
        productLineRepository.save(new ProductLine(productLineRequest.getName(),productLineRequest.getDescription()));

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

//    @GetMapping("/products")
//    public ResponseEntity getAllProduct() {
//        List<Product> products = productRepository.findAll(Sort.by(Sort.Direction.ASC,"productCode"));
//        return ResponseEntity.status(HttpStatus.OK).body(products);
//    }


    @GetMapping("/productlines")
    public ResponseEntity getAllProductLine() {
        List<ProductLine> products = productLineRepository.findAll(Sort.by(Sort.Direction.ASC,"productLine"));
        return ResponseEntity.status(HttpStatus.OK).body(products);
    }

    @GetMapping("/product-lines")
    public ResponseEntity<Page<ProductLine>> getProductLineInNeed(@RequestParam("page") Integer page,
                                                                  @RequestParam("size") Integer size,
                                                                  @RequestParam("sort") String  sort,
                                                                  @RequestParam("direction") String direction) {



        Sort.Direction dir = Sort.Direction.ASC;
        if("desc".equals(direction)){
            dir = Sort.Direction.DESC;
        }
        Pageable pageable = PageRequest.of(page,size,Sort.by(dir,this.nameMap.get(sort)));

        Page<ProductLine> productLines = productLineRepository.findAll(pageable);

        return ResponseEntity.status(HttpStatus.OK).body(productLines);
    }

    @GetMapping("/products")
    public ResponseEntity<Page<Product>> getProductInNeed(Pageable pageable) {

        Page<Product> products = productRepository.findAll(pageable);
        return ResponseEntity.status(HttpStatus.OK).body(products);

    }
}
