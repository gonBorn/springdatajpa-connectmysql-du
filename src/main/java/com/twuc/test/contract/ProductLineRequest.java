package com.twuc.test.contract;

public class ProductLineRequest {

    private String name;
    private String description;

    public ProductLineRequest() {
    }

    public ProductLineRequest(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
