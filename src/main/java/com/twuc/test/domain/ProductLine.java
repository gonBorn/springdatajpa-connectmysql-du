package com.twuc.test.domain;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "productlines")
public class ProductLine {
    @Id
    @Column(nullable = false, length = 50)
    private String productLine;

    @Size(max = 4000)
    @Column
    private String textDescription;

    public String getProductLine() {
        return productLine;
    }

    public String getTextDescription() {
        return textDescription;
    }

    public ProductLine() {
    }

    public ProductLine(String productLine, @Size(max = 4000) String textDescription) {
        this.productLine = productLine;
        this.textDescription = textDescription;
    }

    public void setProductLine(String productLine) {
        this.productLine = productLine;
    }

    public void setTextDescription(String textDescription) {
        this.textDescription = textDescription;
    }
}
